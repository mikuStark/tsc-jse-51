package ru.tsc.karbainova.tm.api.repository;

import ru.tsc.karbainova.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Map;

public interface ICommandRepository {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Map<String, AbstractCommand> getCommands();

    Map<String, AbstractCommand> getArguments();

    Collection<String> getCommandArg();

    Collection<String> getCommandName();

    void create(AbstractCommand command);

}
