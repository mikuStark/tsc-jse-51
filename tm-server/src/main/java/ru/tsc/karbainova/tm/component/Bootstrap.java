package ru.tsc.karbainova.tm.component;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.karbainova.tm.api.repository.*;
import ru.tsc.karbainova.tm.api.service.*;
import ru.tsc.karbainova.tm.api.service.dto.*;
import ru.tsc.karbainova.tm.api.service.model.IUserServiceModel;
import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.comparator.ComparatorCommand;
import ru.tsc.karbainova.tm.constant.TerminalConst;
import ru.tsc.karbainova.tm.endpoint.*;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.AbstractException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.system.UnknowCommandException;
import ru.tsc.karbainova.tm.repository.*;
import ru.tsc.karbainova.tm.service.*;
import ru.tsc.karbainova.tm.service.dto.*;

import javax.xml.ws.Endpoint;
import java.util.stream.Collectors;

public class Bootstrap implements ServiceLocator {

    @Getter
    private final IPropertyService propertyService = new PropertyService();

    private final IConnectionService connectionService = new ConnectionService(propertyService);
    @Getter
    private final IAdminUserService adminUserService = new AdminUserService(connectionService);
    @Getter
    private final IUserService userService = new UserService(connectionService);

    @Getter
    private final IUserServiceModel userServiceModel = new ru.tsc.karbainova.tm.service.model.UserService(connectionService);

    private final ICommandRepository commandRepository = new CommandRepository();
    @Getter
    private final ICommandService commandService = new CommandService(commandRepository);
    @Getter
    private final IProjectService projectService = new ProjectService(connectionService);
    @Getter
    private final ITaskService taskService = new TaskService(connectionService);
    @Getter
    private final IProjectToTaskService projectToTaskService = new ProjectToTaskService(connectionService);

    private final ILogService logService = new LogService();

    private final CalcEndpoint calcEndpoint = new CalcEndpoint();
    @Getter
    private final ISessionService sessionService = new SessionService(connectionService);

    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);

    private final UserEndpoint userEndpoint = new UserEndpoint(this);

    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    private void initEndpoint(final String wsdl, final Object endpoint) {
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void initEndpoint() {
        initEndpoint("http://localhost:8010/CalcEndpoint?wsdl", calcEndpoint);
        initEndpoint("http://localhost:8010/ProjectEndpoint?wsdl", projectEndpoint);
        initEndpoint("http://localhost:8010/TaskEndpoint?wsdl", taskEndpoint);
        initEndpoint("http://localhost:8010/UserEndpoint?wsdl", userEndpoint);
        initEndpoint("http://localhost:8010/AdminUserEndpoint?wsdl", adminUserEndpoint);
        initEndpoint("http://localhost:8010/SessionEndpoint?wsdl", sessionEndpoint);
    }

    public void init() throws Exception {
        displayWelcome();
        initPID();
        initEndpoint();
        initJMS();
//        adminUserService.create("admin", "admin", Role.ADMIN);
    }

    @SneakyThrows
    public void initJMS() {
        @NotNull final BrokerService broker = new BrokerService();
        @NotNull final String bindAddress = "tcp://" + BrokerService.DEFAULT_BROKER_NAME + ":" + BrokerService.DEFAULT_PORT;
        broker.addConnector(bindAddress);
        broker.start();
    }

    @SneakyThrows
    private void initPID() {
        @NonNull final String filename = "task-manager.pid";
        @NonNull final String pid = Long.toString(getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NonNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private static long getPID() {
        final String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        if (processName != null && processName.length() > 0) {
            try {
                return Long.parseLong(processName.split("@")[0]);
            } catch (@NonNull Exception e) {
                return 0;
            }
        }
        return 0;
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

}
